import React, {useState, Fragment, useLayoutEffect} from "react";

const Responsive = (props) => {
    const [size, setSize] = useState('mobile')

    useLayoutEffect(() => {
        const onSizeChange = () => {
            const screenWidth = window.innerWidth

            if (screenWidth <= 480) {
                setSize('mobile')
            } else if (screenWidth > 480 && screenWidth <= 960) {
                setSize('tablet')
            } else {
                setSize('desktop')
            }
        }
        onSizeChange();
        window.addEventListener('resize', onSizeChange)
    }, [])

    return (
        <Fragment>
            <div className="responsive">
                {size === 'mobile' && (
                    <div className="mobile-view">
                        {props.mobileHtml}
                    </div>
                )}

                {size === 'tablet' && (
                    <div className="tablet-view">
                        {props.tabletHtml}
                    </div>
                )}

                {size === 'desktop' && (
                    <div className="desktop-view">
                        {props.desktopHtml}
                    </div>
                )}
            </div>
        </Fragment>
    )
}

export default Responsive