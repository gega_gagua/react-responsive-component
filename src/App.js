import React from "react";
import Responsive from './components/responsive'

const App = () => {
  const mobileHtml = <div><b>Show</b> on mobile</div>
  const tabletHtml = <div><b>Show</b> on tablet</div>
  const desktopHtml = <div><b>Show</b> on desktop</div> // sheidzleba sxva component gvqondes  expoetad da is chavsvat

  return (
    <div className="App">
      <Responsive 
        mobileHtml={mobileHtml}
        tabletHtml={tabletHtml}
        desktopHtml={desktopHtml}
      />
    </div>
  );
}

export default App;
